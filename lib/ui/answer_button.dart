import 'package:flutter/material.dart';


class AnswerButton extends StatelessWidget {
  
  bool answer;
  VoidCallback _onTap;

  AnswerButton(this.answer, this._onTap);

  @override 
  Widget build(BuildContext context){
     return new Expanded ( //true button
            child: new Material( //True Button
              color: answer == true ? Colors.greenAccent : Colors.redAccent,
              child: new InkWell(
                onTap: () => _onTap(),
                child: new Center(
                  child: new Container(
                    decoration: new BoxDecoration(
                      border: new Border.all(color: Colors.white, width: 5.0)
                    ),
                    padding: new EdgeInsets.all(20.0),
                  child: new Text(answer == true ? 'Vrai' : 'Faux',
                  style: new TextStyle(color: Colors.white, fontSize: 40.0, fontWeight: FontWeight.w500, fontStyle: FontStyle.italic ),
                    ),
                  ),
                ),
              ),
            ),
          );
  }
}