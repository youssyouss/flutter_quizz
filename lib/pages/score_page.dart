import 'package:flutter/material.dart';
import './landing.dart';


class  ScorePage extends StatelessWidget {

  final int score; 
  final int totalQuestion; 

  ScorePage(this.score, this.totalQuestion); 


  @override
  Widget build(BuildContext context){
    return new Material(
      color: Colors.pinkAccent[100],
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Text("Score:", style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontSize: 50.0),), 
          new Text(score.toString() + "/" + totalQuestion.toString() , style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontSize: 50.0),), 
          new IconButton(
            icon: new Icon(Icons.arrow_right),
            color: Colors.white,
            iconSize: 60.0,
            onPressed: () => Navigator.of(context).pushAndRemoveUntil(new MaterialPageRoute(builder: (BuildContext context ) => new LandingPage()), (Route route) => route == null),
          )
        ],
      ),
    );
  }
}