import 'package:flutter/material.dart';
import 'package:quizz/ui/question_text.dart';
import '../utils/question.dart';
import '../utils/quizz.dart';
import '../ui/answer_button.dart';
import '../ui/overlay.dart';
import './score_page.dart';

class QuizPage extends StatefulWidget {
  @override
  State createState() => new QuizPageState();
}

class QuizPageState extends State<QuizPage> {
  Question currentQuestion;
  Quizz quiz = new Quizz([
    new Question("Elon Musk est-il un alien?", true),
    new Question(
        "La chanteuse Céline Dion est née dans la ville de \"Charlemagne\" au Québec",
        true),
    new Question(
        "Les connexions Wi-Fi peuvent être perturbées par les fours à micro-ondes?",
        true),
    new Question("L'est de l'Espagne est bordé par l'océan Atlantique.", false),
    new Question(
        "Le \"gogol\" est un nombre représenté par le chiffre 1 suivi de 100 zéros.",
        true),
    new Question(
        "Calamity Jane est uniquement un personnage de fiction.", false),
    new Question(
        "Le premier être vivant à aller dans l'Espace est la chienne Laïka, envoyée par les Américains.",
        false),
    new Question(
        "Le Petit Prince, personnage du conte d'Antoine de Saint-Exupéry, est originaire de l'astéroïde B 612.",
        true),
    new Question(
        " Un éthylabélophile collectionne les étiquettes de bouteilles d'alcool.",
        true),
    new Question(
        "La constellation d'Orion est aussi appelée Le Chasseur.", true)
  ]);
  String questionText;
  int questionNumber;
  bool isCorrect;
  bool overlayVisible = false;

  @override
  void initState() {
    super.initState();
    currentQuestion = quiz.nextQuestion;
    questionText = currentQuestion.question;
    questionNumber = quiz.questionNumber;
  }

  void handleAnswer(bool answer) {
    isCorrect = (currentQuestion.answer == answer);
    quiz.answer(isCorrect);
    this.setState(() {
      overlayVisible = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        new Column(
          //Main Page
          children: <Widget>[
            new AnswerButton(true, () => handleAnswer(true)),
            new QuestionText(questionText, questionNumber),
            new AnswerButton(false, () => handleAnswer(false))
          ],
        ),
        overlayVisible == true
            ? new CorrectWrongOverlay(isCorrect, () {
                if (quiz.length == questionNumber) {
                  Navigator.of(context).pushAndRemoveUntil(
                      new MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new ScorePage(quiz.score, quiz.length)),
                      (Route route) => route == null);
                }
                currentQuestion = quiz.nextQuestion;
                this.setState(() {
                  overlayVisible = false;
                  questionText = currentQuestion.question;
                  questionNumber = quiz.questionNumber;
                });
              })
            : new Container()
      ],
    );
  }
}
