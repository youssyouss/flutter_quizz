import './question.dart';

class Quizz {
  List<Question> _questions; 
  int _currentQuestionIndex = -1;
  int _score = 0;

  Quizz(this._questions) {
    _questions.shuffle(); 
  }

  int get score =>  _score; 
  int get  questionNumber => _currentQuestionIndex + 1;
  int get length  => _questions.length; 
  List<Question> get questions => _questions;
  Question get  nextQuestion {
    _currentQuestionIndex++;
    if(_currentQuestionIndex >= length) return null; 
    return _questions[_currentQuestionIndex]; 
  }
  void answer(bool isCorrect){
    if(isCorrect) _score++;
  }
}