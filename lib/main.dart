import 'package:flutter/material.dart'; 
import './pages/landing.dart';
import './pages/quizPage.dart';
import './pages/score_page.dart';

void main() {
  runApp(new MaterialApp(
    home: new LandingPage(),
    debugShowCheckedModeBanner: false
  ));
}